@extends('layout.app')

@section('title', 'Reset Password')

@section('content')
    <div class="row mt-2">
        <div class="col-sm-4 mx-auto d-table">
            <div class="d-table-cell align-middle">
                <div class="card">
                    <div class="card-body">
                        <div class="text-center my-3">
                            <h1 class="h2">Get Password Reset Link</h1>
                            <p class="lead">
                                by entering your account email
                            </p>
                        </div>
                        <div class="m-sm-4">
                            <form>
                                <div class="mb-3">
                                    <label class="form-label">Email</label>
                                    <input class="form-control form-control-lg" type="email" name="email"
                                        placeholder="Enter your email" />
                                    <small>
                                        <a href="{{ route('login.form') }}">Got password Login?</a>
                                    </small>
                                </div>
                                <div class="text-center mt-3">
                                    <button type="submit" class="btn btn-lg btn-primary">Send Password Reset Link</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
