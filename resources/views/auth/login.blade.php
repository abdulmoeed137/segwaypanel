@extends('layout.app')

@section('title', 'Login')

@section('content')
    <div class="row mt-2">
        <div class="col-sm-4 mx-auto d-table">
            <div class="d-table-cell align-middle">
                <div class="card">
                    <div class="card-body">
                        <div class="text-center my-3">
                            <h1 class="h2">Login</h1>
                            <p class="lead">
                                to your account to continue
                            </p>
                        </div>
                        <div class="m-sm-4">
                            <form onsubmit="onLogin(event)">
                                @csrf
                                <div class="mb-3">
                                    <label class="form-label">Email</label>
                                    <input class="form-control form-control-lg" type="email" name="email"
                                        placeholder="Enter your email" />
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Password</label>
                                    <input class="form-control form-control-lg" type="password" name="password"
                                        placeholder="Enter your password" />
                                    <small>
                                        <a href="{{ route('password.email') }}">Forgot password?</a>
                                    </small>
                                </div>
                                <div>
                                    <label class="form-check">
                                        <input class="form-check-input" type="checkbox" value="1" name="remember"
                                            checked>
                                        <span class="form-check-label">
                                            Remember me
                                        </span>
                                    </label>
                                </div>
                                <div class="text-center mt-3">
                                    <button type="submit" class="btn btn-lg btn-primary">Login</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        async function onLogin(e) {
            const smbBtnEl = e.submitter
            try {
                e.preventDefault()
                smbBtnEl.disabled = true
                const formData = new FormData(e.target)
                const response = await fetch("{{ route('login') }}", {
                    headers: {
                        credentials: "include",
                        accept: "application/json"
                    },
                    method: "POST",
                    body: formData
                });
                const data = await response.json();
                if (!response.ok) {
                    data['status'] = response.status
                    throw data;
                }
                removeErrors()
                if (data.success) {
                    window.location.replace("/panel")
                }

            } catch (errorRes) {
                if (errorRes.status == 422) {
                    setErrors(errorRes.errors);
                } else {
                    console.log(!errorRes.error ? errorRes.message : errorRes.error);
                }
            } finally {
                smbBtnEl.disabled = false
            }
        }
    </script>
@endsection
