@extends('layout.app')

@section('title', 'Data Check')

@section('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.12.1/datatables.min.css" />
@endsection


@section('content')

	<h1 class="h3 mb-3">Data Check</h1>

	<div class="card">
	    <div class="card-body">
	        <div class="row">
	            <form action="{{ route('data-check') }}" method="POST">
	                @csrf
	                <div class="row">
	                    <div class="col-md-3">
	                        <label><strong>Code</strong></label>
	                        <input type="text" name="code" class="form-control" 
	                               value="{{ old('code', $filters['code'] ?? '') }}">
	                    </div>
	                    <div class="col-md-3 mt-4">
	                        <button type="submit" class="btn btn-sm btn-primary">Check</button>
	                    </div>
	                </div>
	            </form>
	        </div>
	        <div class="row mt-4">
	            <div class="table-responsive">
	                <table id="datatable" class="table table-bordered table-striped">
	                    <thead style="background-color: #f8f9fa; border-bottom: 2px solid #ddd;">
	                        <tr>
	                            <th class="text-left">Title</th>
	                            <th class="text-center">Segway {{$version}}</th>
	                            <th class="text-center">Meezotech</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        @if(isset($segway) && isset($meezo))
	                            <tr>
	                                <td><strong>Timestamp</strong></td>
	                                <td class="text-center">{{ $segway['gpsUTCTime'] ?? 'N/A' }}</td>
	                                <td class="text-center">{{ $meezo['gpsUTCTime'] ?? 'N/A' }}</td>
	                            </tr>
	                            <tr>
	                                <td><strong>Date Time</strong></td>
	                                <td class="text-center">{{ $segway['dateTime'] ?? 'N/A' }}</td>
	                                <td class="text-center">{{ $meezo['dateTime'] ?? 'N/A' }}</td>
	                            </tr>

	                            <tr>
	                            	<td colspan="3" class="text-center"><strong>Token Details</strong></td>
	                            </tr>

	                            <tr>
	                            	<td><strong>IOT Type</strong></td>
	                            	<td><strong>Token</strong></td>
	                            	<td><strong>Expiry</strong></td>
	                            </tr>
	                            <tr>
	                            	<td class="text-left">{{ $token['apac']['type'] ?? 'N/A' }}</td>
	                            	<td class="text-left">{{ $token['apac']['token'] ?? 'N/A' }}</td>
	                            	<td class="text-left">{{ $token['apac']['expires_in'] ?? 'N/A' }}</td>
	                            </tr>
	                            <tr>
	                            	<td class="text-left">{{ $token['eu']['type'] ?? 'N/A' }}</td>
	                            	<td class="text-left">{{ $token['eu']['token'] ?? 'N/A' }}</td>
	                            	<td class="text-left">{{ $token['eu']['expires_in'] ?? 'N/A' }}</td>
	                            </tr>
	                        @else
	                            <tr>
	                                <td colspan="3" class="text-center">No data available</td>
	                            </tr>
	                        @endif
	                    </tbody>
	                </table>
	            </div>
	        </div>
	    </div>
	</div>



@endsection


@section('scripts')

	<script src="https://code.jquery.com/jquery-3.6.1.min.js"
      integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.12.1/datatables.min.js"></script>
@endsection