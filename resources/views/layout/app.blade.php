<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Responsive Admin &amp; Dashboard Template based on Bootstrap 5">
    <meta name="author" content="AdminKit">
    <meta name="keywords"
        content="adminkit, bootstrap, bootstrap 5, admin, dashboard, template, responsive, css, sass, html, theme, front-end, ui kit, web">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="shortcut icon" href="{{ asset('img/icons/icon-48x48.png') }}" />

    <link rel="canonical" href="https://demo-basic.adminkit.io/" />

    <title>Panel - @yield('title')</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap" rel="stylesheet">
    @yield('styles')
</head>

<body>
    <div class="wrapper">
        @includeWhen(auth()->check(), 'layout.sidebar')
        <div class="main">
            @includeWhen(auth()->check(), 'layout.header')
            <main class="content">
                <div class="container-fluid p-0">
                    @yield('content')
                </div>
            </main>
            @includeWhen(auth()->check(), 'layout.footer')
        </div>
    </div>

    @yield('modals')
    <script src="{{ asset('js/app.js') }}"></script>
    @yield('scripts')
    <script>
        function setErrors(errors, by = "name", prepend = "") {
            for (const [key, errorMsgs] of Object.entries(errors)) {
                const inputEl = document.querySelector(`[${by}=${prepend+key.replace("_", "-")}]`)
                if (!inputEl) {
                    continue
                }
                inputEl.classList.add("is-invalid")
                for (const error of errorMsgs) {
                    inputEl.insertAdjacentHTML("afterend", `<div class="invalid-feedback">${error}</span>`)
                }
            }
        }

        function removeErrors() {
            const classEls = document.querySelectorAll(".is-invalid")
            const errorEls = document.querySelectorAll(".invalid-feedback")
            if (classEls != null || classEls.length > 0) {
                for (const classEl of classEls) {
                    classEl.classList.remove("is-invalid")
                }
            }
            if (errorEls != null || errorEls.length > 0) {
                for (const errorEl of errorEls) {
                    errorEl.remove()
                }
            }
        }

        function manuplateModal(id, title, body, footerTxt = "submit", formAtr = "", isStore = false) {
            const modal = document.getElementById(id)
            const headerEl = modal.querySelector(".modal-header h1")
            const bodyEl = modal.querySelector(".modal-body")
            const footerEl = modal.querySelector(".modal-footer")

            if (isStore) {
                window["modalEl"] = {}
                modalEl.title = headerEl.textContent.trim()
                modalEl.body = !!body ? (!formAtr ? modalEl.body = bodyEl.innerHTML.trim() :
                    bodyEl.firstElementChild.innerHTML.trim()) : "";
                modalEl.footerTxt = footerEl.firstElementChild.textContent.trim()
                if (typeof formAtr == 'object') {
                    const form = bodyEl.querySelector("form")
                    modalEl.formAtr = form.getAttributeNames().reduce((obj, at) => {
                        obj[at] = form.getAttribute(at)
                        form.removeAttribute(at)
                        return obj
                    }, {})
                }
            }

            headerEl.textContent = title
            footerEl.firstElementChild.textContent = footerTxt
            if (typeof formAtr == 'object') {
                const form = bodyEl.querySelector("form")
                Object.entries(formAtr).forEach(([k, v]) => {
                    form.setAttribute(k, v)
                })
                if (!!formAtr.id) {
                    footerEl.firstElementChild.setAttribute("form", formAtr.id)
                    footerEl.firstElementChild.setAttribute("onclick",
                        `!this.form && document.getElementById("${formAtr.id}").submit()`)
                }
            }
            if (!!body) {
                if (typeof formAtr == 'object') {
                    bodyEl.firstElementChild.innerHTML = body
                } else {
                    bodyEl.innerHTML = body
                }
            }
        }

        function toggleModal(id, cb = null) {
            const modalEl = document.getElementById(id);
            const modal = bootstrap.Modal.getOrCreateInstance(modalEl);
            modal.toggle();
            cb
        }
    </script>
</body>

</html>
