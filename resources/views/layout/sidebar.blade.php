<nav id="sidebar" class="sidebar js-sidebar">
    <div class="sidebar-content js-simplebar">
        <a class="sidebar-brand" href="{{ route('dashboard') }}">
            <span class="align-middle">{{ config('app.name') }}</span>
        </a>

        <ul class="sidebar-nav">
            <li class="sidebar-header">
                Pages
            </li>

            <li class="sidebar-item {{ request()->routeIs('dashboard') ? 'active' : '' }}">
                <a class="sidebar-link" href="{{ route('dashboard') }}">
                    <i class="align-middle" data-feather="sliders"></i> <span class="align-middle">Dashboard</span>
                </a>
            </li>
            <li class="sidebar-item {{ request()->is('panel/iots') || request()->is('panel/iots/*') ? 'active' : '' }}">
                <a data-bs-target="#iots" data-bs-toggle="collapse" class="sidebar-link collapsed">
                    <i class="align-middle" data-feather="layout"></i> <span class="align-middle">Iots</span>
                </a>
                <ul id="iots" class="sidebar-dropdown list-unstyled collapse" data-bs-parent="#sidebar">
                    <li
                        class="sidebar-item {{ request()->routeIs('iots.list') || request()->routeIs('iot.history') ? 'active' : '' }}">
                        <a class="sidebar-link" href="{{ route('iots.list') }}">List</a>
                    </li>
                    <li
                        class="sidebar-item {{ request()->routeIs('iots.history') && request()->has('current') ? 'active' : '' }}">
                        <a class="sidebar-link" href="{{ route('iots.history', ['current' => 1]) }}">History
                            Current</a>
                    </li>
                    <li
                        class="sidebar-item {{ request()->routeIs('iots.history') && !request()->has('current') ? 'active' : '' }}">
                        <a class="sidebar-link" href="{{ route('iots.history') }}">History
                            All
                            {{-- <span class="sidebar-badge badge bg-primary">Pro</span> --}}</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
