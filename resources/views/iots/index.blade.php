@extends('layout.app')

@section('title', 'Iot List')

@section('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.12.1/datatables.min.css" />
@endsection

@section('content')
    <h1 class="h3 mb-3">Iot List</h1>
    <div class="card">
        <div class="card-body">
            <div class="text-end mb-4">
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#commanModal">Add
                    Iot</button>
            </div>
            <div class="table-responsive">
                <table id="datatable">
                    <thead>
                        <tr>
                            <td>S.no</td>
                            <td>Code</td>
                            <td>Created At</td>
                            <td>Action</td>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    <div class="modal fade" id="commanModal" tabindex="-1" aria-labelledby="commanModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="commanModalLabel">Add Iot</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="store-form" onsubmit="manuplateData(event)">
                        <div class="form-group">
                            <input type="text" name="code" id="code" placeholder="Iot code" class="form-control">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="commanModalSubmit" class="btn btn-primary" form="store-form"
                        onclick="!this.form && document.getElementById('modal-form').submit()">Add</button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://code.jquery.com/jquery-3.6.1.min.js"
        integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.12.1/datatables.min.js"></script>
    <script>
        const table = $('#datatable').DataTable({
            processing: true,
            serverSide: true,
            lengthChange: true,
            ajax: async function(dt, callback, settings) {
                await setTimeout(async () => {
                    const page = Math.ceil(settings._iDisplayStart / settings._iDisplayLength) + 1
                    await loadData({
                        page: page,
                        query: dt.search.value.trim(),
                        orderBy: dt.columns[dt.order[0].column].data,
                        dir: dt.order[0].dir,
                        count: dt.length
                    }, callback)
                }, 800)
            },
            columns: [{
                data: "DT_RowIndex"
            }, {
                data: "code",
            }, {
                data: "created_at"
            }, {
                targets: -1,
                data: "code",
                render: function(data) {
                    return `<div class="d-flex justify-content-between align-items-center">
                    <button class='btn btn-sm btn-success' onclick="showModal('${data}', 'update')">Edit</button>
                    <button class='btn btn-sm btn-danger' onclick="showModal('${data}', 'delete')">Delete</button>
                    <a class='btn btn-sm btn-info' href="/panel/iots/${data}/history">History</a></div>`;
                }
            }, ],
            columnDefs: [{
                searchable: false,
                orderable: false,
                targets: [0, -1],
            }, {
                defaultContent: "-",
                targets: "_all"
            }],
            order: [
                [1, 'asc']
            ],
            initComplete: function() {
                var input = $(".dataTables_filter input").unbind(),
                    self = this.api(),
                    $searchButton = $("<button>")
                    .addClass("btn btn-secondary ml-2")
                    .html(
                        '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search align-middle"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>'
                    )
                    .click(function() {
                        self.search(input.val()).draw();
                    })
                $('.dataTables_filter').append($searchButton);
            }
        })
        table.on('draw.dt', function() {
            const PageInfo = table.page.info();
            table.column(0, {
                page: 'current'
            }).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            });
        });

        document.getElementById("commanModal").addEventListener('hidden.bs.modal', e => {
            if (!!window.modalEl) {
                manuplateModal("commanModal", ...Object.values(modalEl))
                delete modalEl
                removeErrors()
                document.getElementById("code").value = ""
            }
        })

        async function loadData(params, callback) {
            try {
                const formData = new FormData()
                formData.append("_token", "{{ csrf_token() }}")
                const response = await fetch(`/api/v1/panel/iots?${new URLSearchParams(params)}`, {
                    headers: {
                        "accept": "application/json"
                    },
                    method: "GET"
                })
                const data = await response.json();
                if (!response.ok) {
                    data['status'] = response.status
                    throw data;
                }
                if (data.success) {
                    const temp = data.iots
                    callback({
                        recordsTotal: temp.total,
                        recordsFiltered: temp.total,
                        data: temp.list
                    });
                }
            } catch (errorRes) {
                console.log(errorRes)
            }
        }

        async function deleteData(e, id) {
            await manuplateData(e, id, "delete")
        }

        async function manuplateData(e, id = "", method = "post") {
            const smbBtnEl = e.submitter
            try {
                e.preventDefault();
                smbBtnEl.disabled = true
                const formData = new FormData(e.target)
                formData.append("_token", "{{ csrf_token() }}")
                if (method != "post") {
                    formData.append("_method", method)
                }
                const response = await fetch(`/api/v1/panel/iots/${id}`, {
                    headers: {
                        "accept": "application/json"
                    },
                    method: "post",
                    body: formData
                })
                const data = await response.json();
                if (!response.ok) {
                    data['status'] = response.status
                    throw data;
                }
                removeErrors()
                if (data.success) {
                    alert(data.message,
                        toggleModal("commanModal"),
                        table.ajax.reload())
                }
            } catch (errorRes) {
                if (errorRes.status == 422) {
                    setErrors(errorRes.errors);
                } else if (errorRes.status == 404) {
                    window.location.replace("/404")
                } else {
                    console.log(!errorRes.error ? errorRes.message : errorRes.error);
                }
            } finally {
                smbBtnEl.disabled = false
            }
        }

        function showModal(id, mode) {
            const params = [
                "commanModal",
                "Update Iot",
                "",
                "Update",
                {
                    id: "update-form",
                    onsubmit: `manuplateData(event, "${id}")`
                },
                true
            ]
            if (mode == "delete") {
                params[1] = "Delete Iot"
                params[2] = "<p>Are you sure you want to delete this iot?</p>"
                params[3] = "Delete"
                params[4] = {
                    id: "delete-form",
                    onsubmit: `deleteData(event, "${id}")`
                }
            } else {
                document.getElementById("code").value = id
            }
            manuplateModal(...params)
            toggleModal(params[0])
        }
    </script>
@endsection
