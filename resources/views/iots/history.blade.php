@extends('layout.app')

@section('title', 'Iot History')

@section('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.12.1/datatables.min.css" />

    <style>
        thead td {
            white-space: nowrap !important;
        }
    </style>
@endsection

@section('content')
    <h1 class="h3 mb-3">{{ !$code ? ($current ? 'Current' : 'All') : $code }} History</h1>
    <div class="card">
        <div class="card-body">
            <div class="d-flex justify-content-between align-items-end mb-4">
                @if (!!$code)
                    <div class="form-group">
                        <label for="start_date">Start Date</label>
                        <input type="datetime-local" name="start_date" id="start-date" class="form-control"
                            min="2015-01-01T00:00:00" onchange="setEndMin(event)">
                    </div>
                    <div class="form-group">
                        <label for="end_date">End Date</label>
                        <input type="datetime-local" name="end_date" id="end-date" class="form-control"
                            onchange="checkIfMin(event)">
                    </div>
                @endif
                @if (!!$code || !!$current)
                    <button type="button" id="load-map" class="btn btn-success {{ !$current ? 'd-none' : '' }}"
                        onclick="mapData(event.target)">Load
                        Map</button>
                    <button type="button" id="deload-map" class="btn btn-success d-none" onclick="hideMap()">Hide
                        Map</button>
                @endif
            </div>
            @if (!!$code)
                <button type="button" id="range-search" class="btn w-100 btn-warning mb-4 d-none"
                    onclick="loadRangeList(event)">Search</button>
            @endif
            @if (!!$code || !!$current)
                <div id="map" class="d-none mb-4" style="height: 500px"></div>
            @endif
            <div class="table-responsive d-none">
                <table id="datatable">
                    <thead>
                        <tr>
                            <td>S.no</td>
                            <td>Code</td>
                            <td>Status Utc Time</td>
                            <td>Latitude</td>
                            <td>Longitude</td>
                            <td>Satellite No</td>
                            <td>Hdop</td>
                            <td>Altitude</td>
                            <td>Gps UTC Time</td>
                            <td>Online</td>
                            <td>Locked</td>
                            <td>Lock Voltage</td>
                            <td>Network Signal</td>
                            <td>Charging</td>
                            <td>Power Percent</td>
                            <td>Speed Mode</td>
                            <td>Speed</td>
                            <td>Odometer</td>
                            <td>Remaining Range</td>
                            <td>Total Riding Secs</td>
                            <td>Created At</td>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDJIqhX4P6Uc99HxWRow9CljXntIQ-Od0g"></script>
    <script src="https://code.jquery.com/jquery-3.6.1.min.js"
        integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.12.1/datatables.min.js"></script>
    <script>
        let start_date = "",
            end_date = "",
            reloadTable;
        const current = window.location.href.includes("?current=1")
        const code = @json($code);

        function setTableData() {
            const table = $('#datatable').DataTable({
                processing: true,
                serverSide: true,
                lengthChange: true,
                ajax: async function(dt, callback, settings) {
                    await setTimeout(async () => {
                        const page = Math.ceil(settings._iDisplayStart / settings._iDisplayLength) +
                            1
                        await loadData({
                            page: page,
                            query: dt.search.value.trim(),
                            orderBy: dt.columns[dt.order[0].column].data,
                            dir: dt.order[0].dir,
                            count: dt.length
                        }, callback)
                    }, 800)
                },
                columns: [{
                    data: "DT_RowIndex"
                }, {
                    data: "code",
                    visible: @json(!$code),
                    searchable: @json(!$code),
                }, {
                    data: "statusUtcTime"
                }, {
                    data: "latitude"
                }, {
                    data: "longitude"
                }, {
                    data: "satelliteNumber"
                }, {
                    data: "hdop"
                }, {
                    data: "altitude"
                }, {
                    data: "gpsUTCTime"
                }, {
                    data: "online"
                }, {
                    data: "locked"
                }, {
                    data: "lockVoltage"
                }, {
                    data: "networkSignal"
                }, {
                    data: "charging"
                }, {
                    data: "powerPercent"
                }, {
                    data: "speedMode"
                }, {
                    data: "speed"
                }, {
                    data: "odometer"
                }, {
                    data: "remainingRange"
                }, {
                    data: "totalRidingSecs"
                }, {
                    data: "created_at"
                }, ],
                columnDefs: [{
                    searchable: false,
                    orderable: false,
                    targets: [0],
                }, {
                    defaultContent: "-",
                    targets: "_all"
                }],
                order: [
                    [20, 'desc']
                ],
                initComplete: function() {
                    var input = $(".dataTables_filter input").unbind(),
                        self = this.api(),
                        $searchButton = $("<button>")
                        .addClass("btn btn-secondary ml-2")
                        .html(
                            '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search align-middle"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>'
                        )
                        .click(function() {
                            self.search(input.val()).draw();
                        })
                    $('.dataTables_filter').append($searchButton);
                }
            })
            table.on('draw.dt', function() {
                const PageInfo = table.page.info();
                table.column(0, {
                    page: 'current'
                }).nodes().each(function(cell, i) {
                    cell.innerHTML = i + 1 + PageInfo.start;
                });
            });

            reloadTable = () => table.ajax.reload()
        }

        async function loadData(params, callback) {
            try {
                if (!!code && (!start_date || !end_date)) {
                    return
                }
                const url = !code ? "/api/v1/panel/iots/history/" : `/api/v1/panel/iots/${code}/history/`
                const formData = new FormData()
                formData.append("_token", "{{ csrf_token() }}")
                if (current) {
                    params['current'] = 1
                } else if (!!code) {
                    params['start_date'] = start_date
                    params['end_date'] = end_date
                }
                const response = await fetch(`${url}?${new URLSearchParams(params)}`, {
                    headers: {
                        "accept": "application/json"
                    },
                    method: "GET"
                })
                const data = await response.json();
                if (!response.ok) {
                    data['status'] = response.status
                    throw data;
                }
                if (data.success) {
                    const temp = data.iot_history
                    callback({
                        recordsTotal: temp.total,
                        recordsFiltered: temp.total,
                        data: temp.list
                    });
                }
            } catch (errorRes) {
                if (errorRes.status == 404) {
                    window.location.replace("/404")
                }
                console.log(errorRes)
            }
        }

        if (!code) {
            setTableData()
            document.querySelector(".table-responsive.d-none").classList.remove("d-none")
        }

        @if (!!$code || !!$current)
            function loadRangeList(e) {
                e.target.disabled = true
                start_date = document.getElementById("start-date").value
                end_date = document.getElementById("end-date").value
                if (!reloadTable) {
                    document.querySelector(".table-responsive.d-none").classList.remove("d-none")
                    setTableData()
                    document.getElementById("load-map").classList.remove("d-none")
                } else {
                    reloadTable()
                }
                if (!document.getElementById("map").classList.contains("d-none")) {
                    mapData()
                }
                e.target.disabled = false
            }

            async function mapData(btn = "") {
                try {
                    btn.disabled = true
                    let part = ""
                    if (!!code && !!start_date && !!end_date) {
                        part = `?start_date=${start_date}&end_date=${end_date}`
                    }
                    await loadMapData(part)
                    if (!!btn) {
                        document.getElementById("load-map").classList.add("d-none")
                        document.getElementById("deload-map").classList.remove("d-none")
                        document.getElementById("map").classList.remove("d-none")
                    }
                } catch (errorRes) {
                    console.log(errorRes)
                } finally {
                    btn.disabled = false
                }
            }

            async function loadMapData(part, reset = false) {
                try {
                    let url = "/api/v1/panel/iots/coordinates"
                    if (!current) {
                        const arr = document.URL.split("/")
                        const code = arr[arr.length - 2]
                        url = `/api/v1/panel/iots/${code}/coordinates${part}`
                    }
                    const formData = new FormData()
                    formData.append("_token", "{{ csrf_token() }}")
                    const response = await fetch(url, {
                        headers: {
                            "accept": "application/json"
                        },
                        method: "GET"
                    })
                    const data = await response.json();
                    if (!response.ok) {
                        data['status'] = response.status
                        throw data;
                    }
                    if (data.success) {
                        if (!part && !reset) {
                            if (!current) resetMapFields();
                        }
                        setMap(data.coordinates, part.includes("?start_date="))
                    }
                } catch (errorRes) {
                    console.log(errorRes)
                }
            }

            function hideMap() {
                document.getElementById("load-map").classList.remove("d-none")
                document.getElementById("deload-map").classList.add("d-none")
                const mapEl = document.getElementById("map")
                mapEl.classList.add("d-none")
                mapEl.innerHTML = ""
            }

            function setMap(coordinates, range = false) {
                const mapEl = document.getElementById("map")
                mapEl.innerHTML = ""
                if (coordinates.length < 1 || (coordinates.length == 1 && Object.values(coordinates[0]).indexOf(0) >= 0)) {
                    mapEl.innerHTML =
                        `<h3 class='text-center'>${!range ? "Incorrect coordinates are being stored.":"No coordinates for the given range."}</h3>`
                    return
                }

                const map = new google.maps.Map(mapEl, {
                    zoom: 14,
                    center: coordinates[0],
                });

                if (!range) {
                    coordinates.forEach((c, k) => {
                        const label = current ? (k + 1) + "" : "CL"
                        const marker = new google.maps.Marker({
                            position: c,
                            label: {
                                text: label,
                                color: "white"
                            },
                            map: map
                        });

                        const infowindow = new google.maps.InfoWindow({
                            content: `<h5>${c.code}</h5>`,
                            ariaLabel: c.code,
                        });
                        marker.addListener("click", () => {
                            infowindow.open({
                                anchor: marker,
                                map,
                            });
                        });
                    })
                } else {
                    setRangeMap(coordinates, map)
                }
            }

            function setRangeMap(coordinates, map) {
                const infowindow = new google.maps.InfoWindow({
                    content: "",
                    ariaLabel: "Location",
                });

                const start = new google.maps.Marker({
                    position: coordinates[0],
                    label: {
                        text: "S",
                        color: "white"
                    },
                    map: map
                });

                if (coordinates.length > 1) {
                    const end = new google.maps.Marker({
                        position: coordinates[coordinates.length - 1],
                        label: {
                            text: "E",
                            color: "white"
                        },
                        map: map
                    });

                    end.addListener("click", () => {
                        infowindow.setContent("<h5>End</h5>")
                        infowindow.open({
                            anchor: end,
                            map,
                        });
                    });
                }


                start.addListener("click", () => {
                    infowindow.setContent("<h5>Start</h5>")
                    infowindow.open({
                        anchor: start,
                        map,
                    });
                });


                const rangePath = new google.maps.Polyline({
                    path: coordinates,
                    geodesic: true,
                    strokeColor: "#FF0000",
                    strokeOpacity: 1.0,
                    strokeWeight: 2,
                });

                rangePath.setMap(map);
            }

            function resetMapFields(loadData = false) {
                document.getElementById("start-date").value = ""
                document.getElementById("end-date").value = ""
                const mapInpSrchEl = document.getElementById("range-search")
                if (!mapInpSrchEl.classList.contains("d-none")) {
                    mapInpSrchEl.classList.add("d-none")
                }
                if (loadData) {
                    loadMapData("", true)
                }
            }

            function setEndMin(e) {
                const endDateEl = document.getElementById("end-date")
                endDateEl.min = e.target.value
                if ((new Date(e.target.value)).getTime() > (new Date(endDateEl.value)).getTime()) {
                    endDateEl.value = ""
                    correctDates(false)
                } else if (!!e.target.value && !!endDateEl.value) {
                    correctDates()
                }
            }

            function checkIfMin(e) {
                const startDateEl = document.getElementById("start-date")
                if ((new Date(e.target.value)).getTime() < (new Date(startDateEl.value)).getTime()) {
                    const temp = e.target.value.split("T")
                    temp[1] = startDateEl.value.split("T")[1]
                    e.target.value = temp.join("T")
                }
                if (!!e.target.value && !!startDateEl.value) correctDates()
            }

            function correctDates(has = true) {
                const mapInpSrchEl = document.getElementById("range-search")
                if (has && mapInpSrchEl.classList.contains("d-none")) {
                    mapInpSrchEl.classList.remove("d-none")
                } else if (!has && !mapInpSrchEl.classList.contains("d-none")) {
                    mapInpSrchEl.classList.add("d-none")
                }
            }
        @endif
    </script>
@endsection
