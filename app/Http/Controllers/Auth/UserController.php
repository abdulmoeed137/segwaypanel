<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    public function login(Request $request)
    {
        try {
            $validated = $request->validate([
                'email' => 'required|email|max:255',
                'password' => 'required|string|min:8'
            ]);

            $user = User::where('email', $validated['email'])->first();
            if (!$user || !Hash::check($validated['password'], $user->password)) {
                throw ValidationException::withMessages([
                    'email' => [trans('auth.failed')],
                ]);
            }

            return response()->json(['token' => $user->createToken($user->email)->plainTextToken, 'message' => "Logged in successfully.", 'success' => true], 200);
        } catch (\Exception $e) {
            $arr = [
                'message' => 'Error',
                'success' => false
            ];
            $code = 500;
            if ($e instanceof ValidationException) {
                $arr['errors'] = array_merge(...array_values($e->errors()))[0];
                $code = 422;
            } else {
                $arr['errors'] = $e->getMessage();
                $arr['trace'] = $e->getTrace();
            }
            return response()->json($arr, $code);
        }
    }

    public function logout(Request $request)
    {
        try {
            $request->user()->currentAccessToken()->delete();
            return response(['message' => 'Logged out successfully', 'success' => true], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Error',
                'success' => false,
                'errors' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 500);
        }
    }
}
