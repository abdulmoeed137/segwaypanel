<?php

namespace App\Http\Controllers;

use App\Models\Iot;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function dashboard()
    {
        $counts = Iot::selectRaw("count(1) as total, cast(sum(case when online = 1 then 1 else 0 END) as unsigned) as online, cast(sum(case when charging = 1 then 1 else 0 END) as unsigned) as charging, cast(sum(case when locked = 1 then 1 else 0 END) as unsigned) as locked")->first();
        return view('dashboard')->with($counts->toArray());
    }

    public function iotList()
    {
        return view('iots.index');
    }

    public function iotHistory(Request $request, $code = '')
    {
        $current = $request->has('current');
        return view('iots.history', compact(['code', 'current']));
    }
}
