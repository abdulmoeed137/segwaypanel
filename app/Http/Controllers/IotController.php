<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\DtRequest;
use App\Http\Resources\PaginateDtResource;
use App\Models\Iot;
use App\Models\IotDetail;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class IotController extends Controller
{
    public function index(DtRequest $request)
    {
        $validated = $request->validated();
        $iots = Iot::when(
            isset($validated['query']),
            fn ($q) => ($q->where('code', 'like', "%{$validated['query']}%")
                ->orWhereRaw("DATE_FORMAT(created_at, '%d-%m-%Y %I:%i %p') like '%{$validated['query']}%'")
            )
        )->when(
            isset($validated['orderBy'], $validated['dir']),
            fn ($q) => $q->orderBy($validated['orderBy'], $validated['dir'])
        )->latest()->paginate($validated['count']);
        return response()->json(['iots' => new PaginateDtResource($iots, 'iot'), 'success' => true], 200);
    }

    public function store(Request $request)
    {
        try {
            $validated = $request->validate([
                'code' => 'required|integer|unique:iots,code'
            ]);
            Iot::create($validated);
            return response()->json(['message' => 'Iot added successfully.', 'success' => true], 201);
        } catch (\Exception $e) {
            $arr = [
                'message' => 'Error',
                'success' => false
            ];
            $code = 500;
            if ($e instanceof ValidationException) {
                $arr['errors'] = $e->errors();
                $code = 422;
            } else {
                $arr['error'] = $e->getMessage();
                $arr['trace'] = $e->getTrace();
            }
            return response()->json($arr, $code);
        }
    }

    public function update(Request $request, $code)
    {
        try {
            $iot = Iot::findOrFail($code);
            $validated = $request->validate([
                'code' => "required|integer|unique:iots,code,$code,code"
            ]);
            $iot->update($validated);
            return response()->json(['message' => 'Iot updated successfully.', 'success' => true], 200);
        } catch (\Exception $e) {
            $arr = [
                'message' => 'Error',
                'success' => false
            ];
            $code = 500;
            if ($e instanceof ValidationException) {
                $arr['errors'] = $e->errors();
                $code = 422;
            } else if ($e instanceof ModelNotFoundException) {
                $arr['error'] = 'Iot not found.';
                $code = 404;
            } else {
                $arr['error'] = $e->getMessage();
                $arr['trace'] = $e->getTrace();
            }
            return response()->json($arr, $code);
        }
    }

    public function destroy($code)
    {
        try {
            $iot = Iot::findOrFail($code);
            $iot->delete();
            return response()->json(['message' => 'Iot deleted successfully.', 'success' => true], 200);
        } catch (\Exception $e) {
            $arr = [
                'message' => 'Error',
                'success' => false
            ];
            $code = 500;
            if ($e instanceof ModelNotFoundException) {
                $arr['error'] = 'Iot not found.';
                $code = 404;
            } else {
                $arr['error'] = $e->getMessage();
                $arr['trace'] = $e->getTrace();
            }
            return response()->json($arr, $code);
        }
    }

    public function history(DtRequest $request, $code = '')
    {
        try {
            $validated = $request->validated();
            $range = $request->validate([
                'start_date' => 'nullable|date',
                'end_date' => 'nullable|required_with:start_date|date|after_or_equal:start_date'
            ]);
            $hasCode = !$request->has('current') && !!$code;
            if ($hasCode) {
                Iot::findOrFail($code);
            }
            $class = '\App\Models\\' . ($request->has('current') ? 'Iot' : 'IotDetail');
            $iots = $class::when(
                $hasCode,
                fn ($q) => $q->where('code', $code)->when(
                    count($range) > 0,
                    fn ($q) => $q->whereBetween('created_at', $range)
                )
            )->when(
                isset($validated['query']),
                fn ($q) => ($q->when(
                    !$hasCode,
                    fn ($q1) => $q1->where('code', 'like', "%{$validated['query']}%")
                        ->orWhere('statusUtcTime', 'like', "%{$validated['query']}%"),
                    fn ($q1) => $q1->where('statusUtcTime', 'like', "%{$validated['query']}%"),
                )
                    ->orWhere('latitude', 'like', "%{$validated['query']}%")
                    ->orWhere('longitude', 'like', "%{$validated['query']}%")
                    ->orWhere('satelliteNumber', 'like', "%{$validated['query']}%")
                    ->orWhere('hdop', 'like', "%{$validated['query']}%")
                    ->orWhere('altitude', 'like', "%{$validated['query']}%")
                    ->orWhere('gpsUTCTime', 'like', "%{$validated['query']}%")
                    ->orWhere('online', 'like', "%{$validated['query']}%")
                    ->orWhere('locked', 'like', "%{$validated['query']}%")
                    ->orWhere('lockVoltage', 'like', "%{$validated['query']}%")
                    ->orWhere('networkSignal', 'like', "%{$validated['query']}%")
                    ->orWhere('charging', 'like', "%{$validated['query']}%")
                    ->orWhere('powerPercent', 'like', "%{$validated['query']}%")
                    ->orWhere('speedMode', 'like', "%{$validated['query']}%")
                    ->orWhere('speed', 'like', "%{$validated['query']}%")
                    ->orWhere('odometer', 'like', "%{$validated['query']}%")
                    ->orWhere('remainingRange', 'like', "%{$validated['query']}%")
                    ->orWhere('totalRidingSecs', 'like', "%{$validated['query']}%")
                    ->when(
                        count($range) < 1,
                        fn ($q1) => $q1->orWhereRaw("DATE_FORMAT(created_at, '%d-%m-%Y %I:%i %p') like '%{$validated['query']}%'")
                    )
                )
            )->when(
                isset($validated['orderBy'], $validated['dir']),
                fn ($q) => $q->orderBy($validated['orderBy'], $validated['dir'])
            )->latest()->paginate($validated['count']);
            return response()->json(['iot_history' => new PaginateDtResource($iots, 'iotHistory'), 'success' => true], 200);
        } catch (\Exception $e) {
            $arr = [
                'message' => 'Error',
                'success' => false
            ];
            $code = 500;
            if ($e instanceof ValidationException) {
                $arr['errors'] = $e->errors();
                $code = 422;
            } else if ($e instanceof ModelNotFoundException) {
                $arr['error'] = 'Iot not found.';
                $code = 404;
            } else {
                $arr['error'] = $e->getMessage();
                $arr['trace'] = $e->getTrace();
            }
            return response()->json($arr, $code);
        }
    }

    public function coordinates(Request $request, $code = '')
    {
        try {
            $validated = $request->validate([
                'start_date' => 'nullable|date',
                'end_date' => 'nullable|required_with:start_date|date|after_or_equal:start_date'
            ]);
            $coordinates = Iot::select('code', 'latitude as lat', 'longitude as lng')
                ->when(!!$code, fn ($q) => $q->findOrFail($code), fn ($q) => $q
                    ->where('latitude', '>', 0)->where('longitude', '>', 0)->get());

            if (!!$code) {
                $coordinates = array($coordinates);
                if (isset($validated['start_date'])) {
                    $coordinates = IotDetail::select('latitude as lat', 'longitude as lng')
                        ->where('code', $code)->whereBetween('created_at', $validated)->groupBy('lat', 'lng')
                        ->orderByRaw('max(created_at) asc')->limit(100)->get();
                }
            }

            return response()->json(['coordinates' => $coordinates, 'success' => true], 200);
        } catch (\Exception $e) {
            $arr = [
                'message' => 'Error',
                'success' => false
            ];
            $code = 500;
            if ($e instanceof ModelNotFoundException) {
                $arr['error'] = 'Iot not found.';
                $code = 404;
            } else {
                $arr['error'] = $e->getMessage();
                $arr['trace'] = $e->getTrace();
            }
            return response()->json($arr, $code);
        }
    }
}
