<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function loginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        try {
            $validated = $request->validate([
                'email' => 'required|email|max:255|exists:users,email',
                'password' => 'required|string|min:8'
            ]);

            if (auth()->attempt($validated, isset($request->remember))) {
                return response()->json([
                    // 'user' => new UserResource(auth()->user()),
                    'message' => "Logged in successfully.", 'success' => true
                ], 200);
            } else {
                throw ValidationException::withMessages([
                    'email' => [trans('auth.failed')],
                ]);
            }
        } catch (\Exception $e) {
            $arr = [
                'message' => 'Error',
                'success' => false
            ];
            $code = 500;
            if ($e instanceof ValidationException) {
                $arr['errors'] = $e->errors();
                $code = 422;
            } else {
                $arr['error'] = $e->getMessage();
                $arr['trace'] = $e->getTrace();
            }
            return response()->json($arr, $code);
        }
    }

    public function logout(Request $request)
    {
        try {
            auth()->logout();
            $request->session()->invalidate();
            $request->session()->regenerateToken();

            return redirect()->away(route('login.form'));
            // return response(['message' => 'Logged out successfully.', 'success' => true], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Error',
                'success' => false,
                'error' => $e->getMessage(),
                'trace' => $e->getTrace()
            ], 500);
        }
    }

    public function passwordEmailForm()
    {
        return view('auth.password.email');
    }

    // public function passwordEmailForm()
    // {
    //     return view('auth.password.email');
    // }
}
