<?php

namespace App\Http\Controllers;

use App\Http\Requests\DtRequest;
use App\Http\Resources\IotDetailResource;
use App\Models\Iot;
use App\Models\IotLocation;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\LazyCollection;
use Illuminate\Validation\ValidationException;

use App\Http\Services\IotService;

use GuzzleHttp\Client;

class ScootyController extends Controller
{
    public function getIotData(Request $request)
    {
        try {
            $request->merge(['count' => $request->count ?: 10]);
            if (isset($request->from, $request->to)) {
                $request->merge([
                    'from' => Carbon::parse(is_numeric($request->from) ? (int) $request->from : $request->from)->toDateTimeString(),
                    'to' => Carbon::parse(is_numeric($request->to) ? (int) $request->to : $request->to)->toDateTimeString()
                ]);
            }
            $rules = [
                'from' => 'nullable|date|required_with:to',
                'to' => 'nullable|date|after_or_equal:from|required_with:from',
                'gap' => 'nullable|integer|min:1|max:59|required_with_all:from,to',
                'code' => 'nullable|exists:iots,code',
            ];
            if ($request->isMethod('post')) {
                array_pop($rules);
                $rules['codes'] = 'required|array|distinct|min:1';
                $rules['codes.*'] = 'required|exists:iots,code';
            }
            $validated = $request->validate(array_merge((new DtRequest())->rules(), $rules));

            if (!isset($validated['code'])) {
                $iot_details = Iot::orderBy('code')->get();
            } else {
                return response()->stream(function () use ($validated) {
                    $iot_details = DB::table('iot_details as ids')
                        ->when(isset($validated['code']), fn ($q) => $q->where('code', $validated['code']))
                        ->when(isset($validated['codes']), fn ($q) => $q->whereIn('code', $validated['codes']))
                        ->when(
                            isset($validated['from'], $validated['to']),
                            fn ($q) => $q->where('created_at', '>=', $validated['from'])
                                ->where('created_at', '<=', $validated['to'])
                                ->when(isset($validated['gap']), fn ($q) => $q->whereRaw("TIMESTAMPDIFF(MINUTE, created_at, '{$validated['from']}') % {$validated['gap']} = 0"))
                        )->orderBy('id', 'desc')->cursor();

                    $firstRecord = true;
                    echo '{"iot_details":[';
                    foreach ($iot_details as $iot_detail) {
                        if (!$firstRecord) {
                            echo ',';
                        }
                        echo json_encode(new IotDetailResource($iot_detail));
                        $firstRecord = false;
                    }
                    echo '], "success":true}';
                }, 200, [
                    'Content-Type' => 'application/json',
                ]);
                // $iot_details =
                //     LazyCollection::make(
                //         fn () =>
                //         IotDetail::when(isset($validated['code']), fn ($q) => $q->where('code', $validated['code']))
                //             ->when(isset($validated['codes']), fn ($q) => $q->whereIn('code', $validated['codes']))
                //             ->when(
                //                 isset($validated['from'], $validated['to']),
                //                 fn ($q) => $q->where('created_at', '>=', $validated['from'])
                //                     ->where('created_at', '<=', $validated['to'])
                //                     ->when(isset($validated['gap']), fn ($q) => $q->whereRaw("TIMESTAMPDIFF(MINUTE, created_at, '{$validated['from']}') % {$validated['gap']} = 0"))
                //             )
                //             // ->when(
                //             //     isset($validated['query']),
                //             //     fn ($q) =>
                //             //     $q->when(
                //             //         !isset($validated['code']) && !isset($validated['codes']),
                //             //         fn ($q) => $q->where('code', 'like', "%{$validated['query']}%")
                //             //             ->orWhere('statusUtcTime', 'like', "%{$validated['query']}%"),
                //             //         fn ($q) => $q->Where('statusUtcTime', 'like', "%{$validated['query']}%")
                //             //     )
                //             //         ->orWhere('latitude', 'like', "%{$validated['query']}%")
                //             //         ->orWhere('longitude', 'like', "%{$validated['query']}%")
                //             //         ->orWhere('satelliteNumber', 'like', "%{$validated['query']}%")
                //             //         ->orWhere('hdop', 'like', "%{$validated['query']}%")
                //             //         ->orWhere('altitude', 'like', "%{$validated['query']}%")
                //             //         ->orWhere('gpsUTCTime', 'like', "%{$validated['query']}%")
                //             //         ->orWhere('lockVoltage', 'like', "%{$validated['query']}%")
                //             //         ->orWhere('networkSignal', 'like', "%{$validated['query']}%")
                //             //         ->orWhere('powerPercent', 'like', "%{$validated['query']}%")
                //             //         ->orWhere('speedMode', 'like', "%{$validated['query']}%")
                //             //         ->orWhere('speed', 'like', "%{$validated['query']}%")
                //             //         ->orWhere('odometer', 'like', "%{$validated['query']}%")
                //             //         ->orWhere('remainingRange', 'like', "%{$validated['query']}%")
                //             //         ->orWhere('totalRidingSecs', 'like', "%{$validated['query']}%")
                //             //         ->orWhereRaw("DATE_FORMAT(created_at, '%d-%m-%Y %I:%i %p') like '%{$validated['query']}%'")
                //             //         ->when(
                //             //             !!preg_match('/^(y|ye|yes|n|no)$/', $validated['query']),
                //             //             function ($q) use ($validated) {
                //             //                 $isTrue = !!preg_match('/^(y|ye|yes)$/', $validated['query']);
                //             //                 return $q->orWhere('online', $isTrue)->orWhere('locked', $isTrue)->orWhere('charging', $isTrue);
                //             //             }
                //             //         )
                //             // )
                //             // ->when(
                //             //     isset($validated['orderBy'], $validated['dir']),
                //             //     fn ($q) => $q->orderBy($validated['orderBy'], $validated['dir'] == 1 ? 'asc' : 'desc')
                //             // )
                //             ->when(!isset($validated['from'], $validated['to']), fn ($q) => $q->latest())
                //     )->all();
                // ->paginate($validated['count']);
            }

            return response()->json(['iot_details' => IotDetailResource::collection($iot_details), 'count' => count($iot_details), 'success' => true], 200);
            // return response()->json(['iot_details' => new PaginateDtResource($iot_details), 'success' => true]);
        } catch (\Exception $e) {
            $arr = [
                'message' => 'Error',
                'success' => false
            ];
            $code = 500;
            if ($e instanceof ValidationException) {
                $arr['errors'] = $e->errors();
                $code = 422;
            } else {
                $arr['errors'] = $e->getMessage();
                $arr['trace'] = $e->getTrace();
            }
            return response()->json($arr, $code);
        }
    }

    public function getIotLocation(Request $request)
    {
        try {
            if (isset($request->from, $request->to)) {
                $request->merge([
                    'from' => Carbon::parse(is_numeric($request->from) ? (int) $request->from : $request->from)->toDateTimeString(),
                    'to' => Carbon::parse(is_numeric($request->to) ? (int) $request->to : $request->to)->toDateTimeString()
                ]);
            }
            $rules = [
                'from' => 'nullable|date|required_with:to',
                'to' => 'nullable|date|after_or_equal:from|required_with:from',
                'gap' => 'nullable|integer|min:1|max:59|required_with_all:from,to',
                'code' => 'required_without:codes|exists:iots,code',
            ];
            if ($request->isMethod('post')) {
                array_pop($rules);
                $rules['codes'] = 'required|array|distinct|min:1';
                $rules['codes.*'] = 'required|exists:iots,code';
            }
            $validated = $request->validate($rules);
            $iot_details =
                LazyCollection::make(
                    fn () =>
                    IotLocation::when(
                        isset($validated['code']),
                        fn ($q) => $q->where('code', $validated['code']),
                        fn ($q) => $q->whereIn('code', $validated['codes'])
                    )->when(
                        isset($validated['from'], $validated['to']),
                        fn ($q) => $q->where('created_at', '>=', $validated['from'])
                            ->where('created_at', '<=', $validated['to'])
                            ->when(isset($validated['gap']), fn ($q) => $q->whereRaw("TIMESTAMPDIFF(MINUTE, created_at, '{$validated['from']}') % {$validated['gap']} = 0"))
                    )->when(!isset($validated['from'], $validated['to']), fn ($q) => $q->latest())->cursor()
                )->all();
            return response()->json([
                'locations' => $iot_details,
                'success' => true
            ], 200);
        } catch (\Exception $e) {
            $arr = [
                'message' => 'Error',
                'success' => false
            ];
            $code = 500;
            if ($e instanceof ValidationException) {
                $arr['errors'] = $e->errors();
                $code = 422;
            } else {
                $arr['errors'] = $e->getMessage();
                $arr['trace'] = $e->getTrace();
            }
            return response()->json($arr, $code);
        }
    }

    public function addIot(Request $request)
    {
        try {
            $validated = $request->validate([
                'code' => 'required|unique:iots,code',
                'version'   =>  'required'
            ]);
            Iot::create($validated);
            return response()->json(['message' => 'Iot added successfully.', 'success' => true, 'device_id' => $validated['code']], 201);
        } catch (\Exception $e) {
            $arr = [
                'message' => 'Error',
                'success' => false
            ];
            $code = 500;
            if ($e instanceof ValidationException) {
                $arr['errors'] = $e->errors();
                $code = 422;
            } else {
                $arr['errors'] = $e->getMessage();
                $arr['trace'] = $e->getTrace();
            }
            return response()->json($arr, $code);
        }
    }

    public function addIotV2(Request $request)
    {
        try {
            $validated = $request->validate([
                'code' => 'required|unique:iots,code',
                'version'   =>  'required'
            ]);
            Iot::create($validated);
            return response()->json(['message' => 'Iot added successfully.', 'success' => true, 'device_id' => $validated['code']], 201);
        } catch (\Exception $e) {
            $arr = [
                'message' => 'Error',
                'success' => false
            ];
            $code = 500;
            if ($e instanceof ValidationException) {
                $arr['errors'] = $e->errors();
                $code = 422;
            } else {
                $arr['errors'] = $e->getMessage();
                $arr['trace'] = $e->getTrace();
            }
            return response()->json($arr, $code);
        }
    }

    public function deleteIot($id)
    {
        try {
            $iot = Iot::findOrFail($id);
            $iot->delete();
            return response()->json(['message' => 'Iot deleted successfully.', 'success' => true, ], 200);
        } catch (\Exception $e) {
            $arr = [
                'message' => 'Error',
                'success' => false
            ];
            $code = 500;
            if ($e instanceof ValidationException) {
                $arr['errors'] = $e->errors();
                $code = 422;
            } else if ($e instanceof ModelNotFoundException) {
                $code = 404;
                $arr['errors'] = 'Iot not found.';
            } else {
                $arr['errors'] = $e->getMessage();
                $arr['trace'] = $e->getTrace();
            }
            return response()->json($arr, $code);
        }
    }

    // public function testToken(){  
    //     $msg = ''; 

    //     $service = new IotService();
    //     $data = $service->getToken(true);
        
    //     if (Carbon::parse($data['expires_in'])->lte(now())) {
    //         $service->retrieveToken();
    //         $msg = 'Apac Token Updated';
    //     }
    //     else{
    //         $msg = 'Apac Token did not update';
    //     }        

    //     $data = $service->getToken(true,'eu');

    //     if (Carbon::parse($data['expires_in'])->lte(now())) {
    //         $service->retrieveToken('eu');
    //         $msg .= ' and EU Token Updated';
    //     }
    //     else{
    //         $msg .= ' and EU Token did not update';
    //     }

    //     return $msg;
    // }

    // public function testIotData(){
    //     $service = new IotService();
    //     $service->retrieveIotDataCopy();
    //     $service->retrieveIotDataCopy('eu');
    //     return 'done';
    //     // return $service->retrieveIotData();
    // }

    public function testIotData($code = null)
    {
        $iot = Iot::where('code', $code)->first();

        if (!$iot) {
            return response()->json(['error' => 'IoT record not found'], 404);
        }

        $version = ($iot->version == 2) ? "eu" : null;

        $service = new IotService();
        $service->retrieveIotDataCopy($version,$code);

        return "<br>done";
    }

    public function checkIotData_(Request $request, $code = null)
    {
        $filters = $request->all();
        $iot = Iot::where('code', $filters['code'])->first();
        $version = ($iot->version == 2) ? "eu" : null;

        $meezo = array();
        $segway = array();

        if ($iot) {
            $meezo['gpsUTCTime']    =   $iot->gpsUTCTime;
            $meezo['dateTime']      =   date("Y-m-d H:i:s", ($iot->gpsUTCTime/1000));
            
            $service = new IotService();
            $segwayAPI = $service->retrieveIotDataCopy($version,$iot->code);

            $segway['gpsUTCTime']   =   $segwayAPI[$iot->code]['gpsUTCTime'];
            $segway['dateTime']     =   date("Y-m-d H:i:s", ($segwayAPI[$iot->code]['gpsUTCTime']/1000));
        }
    
        return view('dataCheck.index',compact('filters','meezo','segway'));
    }

    public function checkIotData(Request $request)
    {
        $request->validate([
            'code' => 'nullable|string',
        ]);

        $filters = $request->all();
        $code = $filters['code'] ?? null;

        $meezo = [];
        $segway = [];
        $version = null;
        
        if ($code) {
            $iot = Iot::where('code', $code)->first();

            if ($iot) {
                $version = $iot->version == 2 ? "eu" : "apac";

                $meezo = $this->prepareMeezoData($iot);

                try {
                    $segway = $this->fetchSegwayData($iot, $version);
                } catch (\Exception $e) {
                    return back()->withErrors(['error' => 'Failed to retrieve IoT data: ' . $e->getMessage()]);
                }
            }
        }

        $token['apac'] = $this->generateToken("apac");
        $token['eu'] = $this->generateToken("eu");

        return view('dataCheck.index', compact('filters', 'meezo', 'segway', 'token', 'version'));
    }

    private function prepareMeezoData($iot)
    {
        return [
            'gpsUTCTime' => $iot->gpsUTCTime,
            'dateTime' => $this->convertToDateTime($iot->gpsUTCTime),
        ];
    }

    private function fetchSegwayData($iot, $version)
    {
        $version_type = ($version == 'eu') ? $version : null;
        $service = new IotService();
        $segwayAPI = $service->retrieveIotDataCopy($version_type, $iot->code);

        return [
            'gpsUTCTime' => $segwayAPI[$iot->code]['gpsUTCTime'] ?? 'N/A',
            'dateTime' => isset($segwayAPI[$iot->code]['gpsUTCTime'])
                ? $this->convertToDateTime($segwayAPI[$iot->code]['gpsUTCTime'])
                : 'N/A',
        ];
    }

    private function generateToken($version)
    {
        $version_type = ($version == 'eu') ? $version : null;
        $service = new IotService();
        $token = $service->getToken(true, $version_type);
        $token['type'] = $version;
        return $token;
    }

    private function convertToDateTime($timestamp)
    {
        return $timestamp ? date("Y-m-d H:i:s", ($timestamp / 1000)) : 'Invalid timestamp';
    }





}
