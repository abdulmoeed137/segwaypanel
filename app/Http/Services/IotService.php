<?php

namespace App\Http\Services;

use App\Models\Iot;
use App\Models\IotDetail;
use App\Models\IotLocation;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class IotService
{
    public function retrieveIotData($type=null)
    {
        $iotType = (isset($type)) ? 'iot-eu' : 'iot';
        $version = (isset($type)) ? 2 : 1;
        $client = new Client([
            'base_uri' => config('app.'.$iotType.'.url'),
            'headers' => [
                'Authorization' => "bearer {$this->getToken(false,$type)}",
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ],
            'verify'    =>  false
        ]);
        
        $iots = Iot::where('version',$version)->get();

        $data = $iots->mapWithKeys(fn ($i) => [$i->code => [
            'created_at' => now()->toDateTimeString(),
            'updated_at' => now()->toDateTimeString()
        ]]);
        $apis = $iots->flatMap(fn ($iot) => Collect(['status', 'location'])->map(fn ($i) =>
        "api/v2/vehicle/query/current/{$i}?iotCode={$iot['code']}"));
        $requests = function () use ($apis, $client) {
            foreach ($apis as $api) {
                yield fn () => $client->getAsync($api);
            }
        };
        $pool = new Pool($client, $requests(), [
            'concurrency' => 15,
            'fulfilled' => function (Response $response, $index) use (&$data, $apis) {
                if ($response->getStatusCode() == 200) {
                    $result = json_decode((string) $response->getBody());
                    if (!!$result->data) {
                        $arr = (array) $result->data;
                        $data[$result->data->iotCode] = array_merge($arr, $data[$result->data->iotCode]);
                    } else {
                        Log::error($response->getBody(), ['api' => $apis[$index]]);
                    }
                }
            },
            // 'rejected' => function (RequestException $reason, $index) use ($apis) {
            //     Log::error($reason->getResponse()->getReasonPhrase(), [
            //         ['api' => $apis[$index]]
            //     ]);
            // },
            'rejected' => function (RequestException $reason, $index) use ($apis) {
                if ($reason->hasResponse()) {
                    Log::error($reason->getResponse()->getReasonPhrase(), [
                        'api' => $apis[$index]
                    ]);
                } else {
                    Log::error("Request failed with no response", [
                        'api' => $apis[$index],
                        'error' => $reason->getMessage()
                    ]);
                }
            },
        ]);
        $pool->promise()->wait();
        $data = $data->filter(fn ($items) => count($items) > 2)
            ->map(function ($items) use ($iots) {
                if (count($items) < 21) {
                    if (isset($items['latitude'])) {
                        $items = array_merge($items, [
                            'online' => 0,
                            'locked' => 0,
                            'lockVoltage' => 0,
                            'networkSignal' => 0,
                            'charging' => 0,
                            'powerPercent' => 0,
                            'speedMode' => 0,
                            'speed' => 0,
                            'odometer' => 0,
                            'remainingRange' => 0,
                            'totalRidingSecs' => 0
                        ]);
                    } else {
                        $items = array_merge($items, [
                            'latitude' => 0,
                            'longitude' => 0,
                            'satelliteNumber' => 0,
                            'hdop' => 0,
                            'altitude' => 0,
                            'gpsUTCTime' => 0
                        ]);
                    }
                }
                $temp = $items;
                unset($temp['iotCode']);
                $iots->find($items['iotCode'])->update($temp + ['created_at' => now(), 'updated_at' => now()]);
                $items['code'] = $items['iotCode'];
                unset($items['iotCode']);
                $loc = IotLocation::where('code', $items['code'])
                    ->where('latitude', $items['latitude'])
                    ->where('longitude', $items['longitude'])->first();
                if (!!$loc) {
                    $loc->update(['gpsUTCTime' => $items['gpsUTCTime'], 'created_at' => now()]);
                } else {
                    IotLocation::insert([
                        'code' => $items['code'],
                        'latitude' => $items['latitude'],
                        'longitude' => $items['longitude'],
                        'gpsUTCTime' => $items['gpsUTCTime'],
                        'altitude' => $items['altitude'],
                        'created_at' => now(),
                        'updated_at' => now()
                    ]);
                }
                return $items;
            })
            ->toArray();
        if (count($data) > 0) {
            IotDetail::insert(array_values($data));
            return true;
        } else {
            return false;
        }
    }

    public function retrieveIotDataCopy($type=null, $code=null)
    {
        $iotType = (isset($type)) ? 'iot-eu' : 'iot';
        $version = (isset($type)) ? 2 : 1;
        $client = new Client([
            'base_uri' => config('app.'.$iotType.'.url'),
            'headers' => [
                'Authorization' => "bearer {$this->getToken(false,$type)}",
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ],
            'verify'    =>  false
        ]);
        
        $iots = Iot::where('version',$version)->where('code',$code)->get();

        $data = $iots->mapWithKeys(fn ($i) => [$i->code => [
            'created_at' => now()->toDateTimeString(),
            'updated_at' => now()->toDateTimeString()
        ]]);

        $apis = $iots->flatMap(fn ($iot) => Collect(['status', 'location'])->map(fn ($i) =>
        "api/v2/vehicle/query/current/{$i}?iotCode={$iot['code']}"));

        $requests = function () use ($apis, $client) {
            foreach ($apis as $api) {
                yield fn () => $client->getAsync($api);
            }
        };

        $pool = new Pool($client, $requests(), [
            'concurrency' => 15,
            'fulfilled' => function (Response $response, $index) use (&$data, $apis) {
                if ($response->getStatusCode() == 200) {
                    $result = json_decode((string) $response->getBody());
                    if (!!$result->data) {
                        $arr = (array) $result->data;
                        $data[$result->data->iotCode] = array_merge($arr, $data[$result->data->iotCode]);
                    } else {
                        Log::error($response->getBody(), ['api' => $apis[$index]]);
                    }
                }
            },
            // 'rejected' => function (RequestException $reason, $index) use ($apis) {
            //     Log::error($reason->getResponse()->getReasonPhrase(), [
            //         ['api' => $apis[$index]]
            //     ]);
            // },
            'rejected' => function (RequestException $reason, $index) use ($apis) {
                if ($reason->hasResponse()) {
                    Log::error($reason->getResponse()->getReasonPhrase(), [
                        'api' => $apis[$index]
                    ]);
                } else {
                    Log::error("Request failed with no response", [
                        'api' => $apis[$index],
                        'error' => $reason->getMessage()
                    ]);
                }
            },
        ]);
        $pool->promise()->wait();
        $data = $data->filter(fn ($items) => count($items) > 2)
            ->map(function ($items) use ($iots) {
                if (count($items) < 21) {
                    if (isset($items['latitude'])) {
                        $items = array_merge($items, [
                            'online' => 0,
                            'locked' => 0,
                            'lockVoltage' => 0,
                            'networkSignal' => 0,
                            'charging' => 0,
                            'powerPercent' => 0,
                            'speedMode' => 0,
                            'speed' => 0,
                            'odometer' => 0,
                            'remainingRange' => 0,
                            'totalRidingSecs' => 0
                        ]);
                    } else {
                        $items = array_merge($items, [
                            'latitude' => 0,
                            'longitude' => 0,
                            'satelliteNumber' => 0,
                            'hdop' => 0,
                            'altitude' => 0,
                            'gpsUTCTime' => 0
                        ]);
                    }
                }
                $temp = $items;
                unset($temp['iotCode']);
                // $iots->find($items['iotCode'])->update($temp + ['created_at' => now(), 'updated_at' => now()]);
                $items['code'] = $items['iotCode'];
                unset($items['iotCode']);
                $loc = IotLocation::where('code', $items['code'])
                    ->where('latitude', $items['latitude'])
                    ->where('longitude', $items['longitude'])->first();
                
                return $items;
            })
            ->toArray();
        if (count($data) > 0) {
            return $data;
        } else {
            return false;
        }
    }

    public function retrieveToken($type=null)
    {
        $iotType = (isset($type)) ? 'iot-eu' : 'iot';
        $client = new Client([
            'base_uri' => config('app.'.$iotType.'.url'),
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'verify'    =>  false
        ]);

        $resp = $client->postAsync('oauth/token', [
            'form_params' => config('app.'.$iotType.'.token')
        ])->wait();
        $result = json_decode((string) $resp->getBody());
        return $this->setToken($result,$type);
    }

    public function getToken($all = false,$type=null)
    {
        $tokenType = (isset($type)) ? '-eu' : '';

        if (storage::exists('token'.$tokenType.'.json')) {
            $data = json_decode(Storage::get('token'.$tokenType.'.json'), true);
        } else {
            $data = $this->retrieveToken($type);
        }
        return !$all ? $data['token'] : $data;
    }

    private function setToken($data,$type=null)
    {
        $iotType = (isset($type)) ? '-eu' : '';
        $temp = [
            'token' =>  $data->access_token,
            'expires_in' => (string) now()->addSeconds($data->expires_in)
        ];
        Storage::put('token'.$iotType.'.json', json_encode($temp));
        return $temp;
    }
}
