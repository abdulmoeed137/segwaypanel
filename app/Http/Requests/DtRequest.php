<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DtRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'count' => $this->count ?: 10,
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'query' => 'nullable|string|max:255',
            'orderBy' => 'nullable|string',
            'dir' => 'nullable|string|in:asc,desc',
            'count' => 'nullable|integer|min:1'
        ];
    }
}
