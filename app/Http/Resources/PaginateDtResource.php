<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PaginateDtResource extends JsonResource
{
    protected $model;
    public function __construct($resource, $model)
    {
        parent::__construct($resource);
        $this->resource = $resource;
        $this->model = '\App\Http\Resources\\' . ucfirst($model) . 'Resource';
    }
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'list' => $this->model::collection($this->items()),
            'total' => $this->total()
        ];
    }
}
