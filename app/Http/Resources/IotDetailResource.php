<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class IotDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->when(isset($this->id), $this->id),
            'iotCode' => $this->code,
            'online' => $this->online == 1,
            'locked' => $this->locked == 1,
            'lockVoltage' => $this->lockVoltage,
            'networkSignal' => $this->networkSignal,
            'charging' => $this->charging == 1,
            'powerPercent' => $this->powerPercent,
            'speedMode' => $this->speedMode,
            'speed' => $this->speed,
            'odometer' => $this->odometer,
            'remainingRange' => $this->remainingRange,
            'totalRidingSecs' => $this->totalRidingSecs,
            'statusUtcTime' => $this->statusUtcTime,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'satelliteNumber' => $this->satelliteNumber,
            'hdop' => $this->hdop,
            'altitude' => $this->altitude,
            'gpsUTCTime' => $this->gpsUTCTime
        ];
    }
}
