<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class IotHistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'code' => $this->code,
            'statusUtcTime' => $this->statusUtcTime,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'satelliteNumber' => $this->satelliteNumber,
            'hdop' => $this->hdop,
            'altitude' => $this->altitude,
            'gpsUTCTime' => $this->gpsUTCTime,
            'online' => $this->online,
            'locked' => $this->locked,
            'lockVoltage' => $this->lockVoltage,
            'networkSignal' => $this->networkSignal,
            'charging' => $this->charging,
            'powerPercent' => $this->powerPercent,
            'speedMode' => $this->speedMode,
            'speed' => $this->speed,
            'odometer' => $this->odometer,
            'remainingRange' => $this->remainingRange,
            'totalRidingSecs' => $this->totalRidingSecs,
            'created_at' => $this->updated_at
        ];
    }
}
