<?php

namespace App\Console\Commands;

use App\Http\Services\IotService;
use Illuminate\Console\Command;

class RetrieveIotData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'retrieve:iot-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $service = new IotService();
        $service->retrieveIotData();
        $service->retrieveIotData('eu');
        return 'done';
    }
}
