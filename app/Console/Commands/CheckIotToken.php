<?php

namespace App\Console\Commands;

use App\Http\Services\IotService;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CheckIotToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:iot-token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $msg = ''; 
             
        $service = new IotService();
        $data = $service->getToken(true);
        
        if (Carbon::parse($data['expires_in'])->lte(now())) {
            $service->retrieveToken();
            $msg = 'Apac Token Updated';
        }
        else{
            $msg = 'Apac Token did not update';
        }        

        $data = $service->getToken(true,'eu');

        if (Carbon::parse($data['expires_in'])->lte(now())) {
            $service->retrieveToken('eu');
            $msg .= ' and EU Token Updated';
        }
        else{
            $msg .= ' and EU Token did not update';
        }

        return $msg;
    }
}
