<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Iot extends Model
{
    use HasFactory;
    protected $primaryKey = 'code';
    protected $fillable = [
        'code',
        'statusUtcTime',
        'latitude',
        'longitude',
        'satelliteNumber',
        'hdop',
        'altitude',
        'gpsUTCTime',
        'online',
        'locked',
        'lockVoltage',
        'networkSignal',
        'charging',
        'powerPercent',
        'speedMode',
        'speed',
        // 'odometer',
        'remainingRange',
        'totalRidingSecs',
        'version'
    ];

    public function getCreatedAtAttribute($value)
    {
        return !$value ? $value : Carbon::parse($value)->format('d-m-Y h:i A');
    }

    public function getUpdatedAtAttribute($value)
    {
        return !$value ? $value : Carbon::parse($value)->format('d-m-Y h:i A');
    }
}
