<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iot_locations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('code')->references('code')->on('iots')->onDelete('cascade');
            $table->double('latitude')->default(0);
            $table->double('longitude')->default(0);
            $table->float('altitude')->default(0);
            $table->double('gpsUTCTime')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('iot_locations');
    }
};
