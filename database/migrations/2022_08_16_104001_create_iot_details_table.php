<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iot_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('code')->constrained('iots', 'code')->onDelete('cascade');
            $table->boolean('online')->default(0);
            $table->boolean('locked')->default(0);
            $table->float('lockVoltage')->default(0);
            $table->float('networkSignal')->default(0);
            $table->boolean('charging')->default(0);
            $table->float('powerPercent')->default(0);
            $table->integer('speedMode')->default(0);
            $table->float('speed')->default(0);
            $table->float('odometer')->default(0);
            $table->float('remainingRange')->default(0);
            $table->float('totalRidingSecs')->default(0);
            $table->string('statusUtcTime')->default(0);
            $table->double('latitude')->default(0);
            $table->double('longitude')->default(0);
            $table->integer('satelliteNumber')->default(0);
            $table->float('hdop')->default(0);
            $table->float('altitude')->default(0);
            $table->double('gpsUTCTime')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('iot_details');
    }
};
