<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('iots', function (Blueprint $table) {
            $table->boolean('online')->default(0)->after('code');
            $table->boolean('locked')->default(0)->after('code');
            $table->float('lockVoltage')->default(0)->after('code');
            $table->float('networkSignal')->default(0)->after('code');
            $table->boolean('charging')->default(0)->after('code');
            $table->float('powerPercent')->default(0)->after('code');
            $table->integer('speedMode')->default(0)->after('code');
            $table->float('speed')->default(0)->after('code');
            $table->float('odometer')->default(0)->after('code');
            $table->float('remainingRange')->default(0)->after('code');
            $table->float('totalRidingSecs')->default(0)->after('code');
            $table->string('statusUtcTime')->default(0)->after('code');
            $table->double('latitude')->default(0)->after('code');
            $table->double('longitude')->default(0)->after('code');
            $table->integer('satelliteNumber')->default(0)->after('code');
            $table->float('hdop')->default(0)->after('code');
            $table->float('altitude')->default(0)->after('code');
            $table->double('gpsUTCTime')->default(0)->after('code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('iots', function ($table) {
            $table->dropColumn('online');
            $table->dropColumn('locked');
            $table->dropColumn('lockVoltage');
            $table->dropColumn('networkSignal');
            $table->dropColumn('charging');
            $table->dropColumn('powerPercent');
            $table->dropColumn('speedMode');
            $table->dropColumn('speed');
            $table->dropColumn('odometer');
            $table->dropColumn('remainingRange');
            $table->dropColumn('totalRidingSecs');
            $table->dropColumn('statusUtcTime');
            $table->dropColumn('latitude');
            $table->dropColumn('longitude');
            $table->dropColumn('satelliteNumber');
            $table->dropColumn('hdop');
            $table->dropColumn('altitude');
            $table->dropColumn('gpsUTCTime');
        });
    }
};
