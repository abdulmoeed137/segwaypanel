<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'panel'], function () {
    Route::group(['middleware' => 'guest'], function () {
        Route::get('login', 'AuthController@loginForm')->name('login.form');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('password/reset', 'AuthController@passwordEmailForm')->name('password.email');
    });

    Route::group(['middleware' => 'auth'], function () {
        Route::post('logout', 'AuthController@logout')->name('logout');
        Route::get('/', 'HomeController@dashboard')->name('dashboard');
        Route::get('iots', 'HomeController@iotList')->name('iots.list');
        Route::get('iots/history', 'HomeController@iotHistory')->name('iots.history');
        Route::get('iots/{code}/history', 'HomeController@iotHistory')->name('iot.history');

        Route::any('data-check/{code?}','ScootyController@checkIotData')->name('data-check');
    });
});



// Route::get('token', 'ScootyController@testToken')->name('token');
// Route::get('data/{code?}', 'ScootyController@testIotData')->name('data');


Route::get('/clear-cache', function () {
    Auth::logout();
    Session::flush();
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    Artisan::call('config:clear');

    return "cleared";
});