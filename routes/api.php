<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
    Route::group(['middleware' => 'guest:api'], function () {
        Route::post('login', 'Auth\UserController@login');
    });

    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('logout', 'Auth\UserController@logout');
        Route::get('iots/detials', 'ScootyController@getIotData');
        Route::post('iots/codes/detials', 'ScootyController@getIotData');
        Route::get('iot/locations', 'ScootyController@getIotLocation');
        Route::post('iot/codes/locations', 'ScootyController@getIotLocation');
        Route::post('iots', 'ScootyController@addIot');
        Route::post('iots/{iot}/delete', 'ScootyController@deleteIot');
    });

    Route::group(['prefix' => 'panel'], function () {
        Route::get('iots', 'IotController@index');
        Route::get('iots/history', 'IotController@history');
        Route::get('iots/{code}/history', 'IotController@history');
        Route::post('iots', 'IotController@store');
        Route::post('iots/{code}', 'IotController@update');
        Route::delete('iots/{code}', 'IotController@destroy');
        Route::get('iots/{code}/coordinates', 'IotController@coordinates');
        Route::get('iots/coordinates', 'IotController@coordinates');
    });
});

Route::group(['prefix' => 'v2'], function () {
    Route::group(['middleware' => 'guest:api'], function () {
        Route::post('login', 'Auth\UserController@login');
    });

    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('logout', 'Auth\UserController@logout');
        Route::get('iots/detials', 'ScootyController@getIotData');
        Route::post('iots/codes/detials', 'ScootyController@getIotData');
        Route::get('iot/locations', 'ScootyController@getIotLocation');
        Route::post('iot/codes/locations', 'ScootyController@getIotLocation');
        Route::post('iots', 'ScootyController@addIotV2');
        Route::post('iots/{iot}/delete', 'ScootyController@deleteIot');
    });
    
});


